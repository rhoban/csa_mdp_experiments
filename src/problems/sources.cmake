set (SOURCES
#TODO: Fix problems with the new multi action setup
#  approach.cpp
#  cart_pole.cpp
#  simulated_cart_pole.cpp
#  cart_pole_stabilization.cpp
  double_integrator.cpp
#  inverted_pendulum.cpp
#  double_inverted_pendulum.cpp
  extended_problem_factory.cpp
  polar_approach.cpp
  kick_controler.cpp
#  kick_optimizer.cpp
#  multi_kick_single_player.cpp
#  one_player_kick.cpp
)